<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 3/25/2022
 * Time: 4:18 PM
 */
?>
<div class="axtronic-news">
    <div class="container">
        <h2 class="heading-title ">Recent News</h2>
        <div class="swiper-slider-news swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="axtronic-post">
                        <div class="post-thumbnail">
                            <div class="posted-on-square">
                                <b>19</b>Feb
                            </div>
                            <a href="">
                                <img src="{{ theme_url('Axtronic/images/new1.png') }}" alt="Axtronic WooCommerce">
                            </a>
                        </div>
                        <div class="entry-content">
                            <div class="entry-meta">
                                <div class="post-author">
                                    <span class="label">By </span>
                                    <a href="#" rel="author">
                                        <span class="author_name">admin </span>
                                    </a>
                                </div>
                                <div class="meta-categories">
                                    <a href="#" rel="category tag">Tips &amp; Tricks</a>,
                                    <a href="" rel="category tag">Uncategorized</a>
                                </div>
                            </div>
                            <h2 class="entry-title">
                                <a href="#" rel="bookmark">The 2017 Wine Lover’s Guide to a Kitchen Remodel</a>
                            </h2>
                            <p>Lorem ipsum dolor sit amet scelerisque orci. Aenean et ex ut elit tincidunt rutrum vitae eleifend metusareil Nunc tincidunt venenatis tellus euismod fermentum. Maecenas sed dapibus eros. Phasellus eu mi metusdajn. Nunc mi nisl, viverra id sollicitudin et, auctor sit amet augue.</p>

                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="axtronic-post">
                        <div class="post-thumbnail">
                            <div class="posted-on-square">
                                <b>19</b>Feb
                            </div>
                            <a href="">
                                <img src="{{ theme_url('Axtronic/images/new2.png') }}" alt="Axtronic WooCommerce">
                            </a>
                        </div>
                        <div class="entry-content">
                            <div class="entry-meta">
                                <div class="post-author">
                                    <span class="label">By </span>
                                    <a href="#" rel="author">
                                        <span class="vcard author author_name">admin </span>
                                    </a>
                                </div>
                                <div class="meta-categories">
                                    <a href="#" rel="category tag">Tips &amp; Tricks</a>,
                                    <a href="" rel="category tag">Uncategorized</a>
                                </div>
                            </div>
                            <h2 class="entry-title">
                                <a href="#" rel="bookmark">The 2017 Wine Lover’s Guide to a Kitchen Remodel</a>
                            </h2>
                            <p>Lorem ipsum dolor sit amet scelerisque orci. Aenean et ex ut elit tincidunt rutrum vitae eleifend metusareil Nunc tincidunt venenatis tellus euismod fermentum. Maecenas sed dapibus eros. Phasellus eu mi metusdajn. Nunc mi nisl, viverra id sollicitudin et, auctor sit amet augue.</p>

                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="axtronic-post">
                        <div class="post-thumbnail">
                            <div class="posted-on-square">
                                <b>19</b>Feb
                            </div>
                            <a href="">
                                <img src="{{ theme_url('Axtronic/images/new3.png') }}" alt="Axtronic WooCommerce">
                            </a>
                        </div>
                        <div class="entry-content">
                            <div class="entry-meta">
                                <div class="post-author">
                                    <span class="label">By </span>
                                    <a href="#" rel="author">
                                        <span class="author_name">admin </span>
                                    </a>
                                </div>
                                <div class="meta-categories">
                                    <a href="#" rel="category tag">Tips &amp; Tricks</a>,
                                    <a href="" rel="category tag">Uncategorized</a>
                                </div>
                            </div>
                            <h2 class="entry-title">
                                <a href="#" rel="bookmark">The 2017 Wine Lover’s Guide to a Kitchen Remodel</a>
                            </h2>
                            <p>Lorem ipsum dolor sit amet scelerisque orci. Aenean et ex ut elit tincidunt rutrum vitae eleifend metusareil Nunc tincidunt venenatis tellus euismod fermentum. Maecenas sed dapibus eros. Phasellus eu mi metusdajn. Nunc mi nisl, viverra id sollicitudin et, auctor sit amet augue.</p>

                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="axtronic-post">
                        <div class="post-thumbnail">
                            <div class="posted-on-square">
                                <b>19</b>Feb
                            </div>
                            <a href="">
                                <img src="{{ theme_url('Axtronic/images/new1.png') }}" alt="Axtronic WooCommerce">
                            </a>
                        </div>
                        <div class="entry-content">
                            <div class="entry-meta">
                                <div class="post-author">
                                    <span class="label">By </span>
                                    <a href="#" rel="author">
                                        <span class="author_name">admin </span>
                                    </a>
                                </div>
                                <div class="meta-categories">
                                    <a href="#" rel="category tag">Tips &amp; Tricks</a>,
                                    <a href="" rel="category tag">Uncategorized</a>
                                </div>
                            </div>
                            <h2 class="entry-title">
                                <a href="#" rel="bookmark">The 2017 Wine Lover’s Guide to a Kitchen Remodel</a>
                            </h2>
                            <p>Lorem ipsum dolor sit amet scelerisque orci. Aenean et ex ut elit tincidunt rutrum vitae eleifend metusareil Nunc tincidunt venenatis tellus euismod fermentum. Maecenas sed dapibus eros. Phasellus eu mi metusdajn. Nunc mi nisl, viverra id sollicitudin et, auctor sit amet augue.</p>

                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="axtronic-post">
                        <div class="post-thumbnail">
                            <div class="posted-on-square">
                                <b>19</b>Feb
                            </div>
                            <a href="">
                                <img src="{{ theme_url('Axtronic/images/new2.png') }}" alt="Axtronic WooCommerce">
                            </a>
                        </div>
                        <div class="entry-content">
                            <div class="entry-meta">
                                <div class="post-author">
                                    <span class="label">By </span>
                                    <a href="#" rel="author">
                                        <span class="vcard author author_name">admin </span>
                                    </a>
                                </div>
                                <div class="meta-categories">
                                    <a href="#" rel="category tag">Tips &amp; Tricks</a>,
                                    <a href="" rel="category tag">Uncategorized</a>
                                </div>
                            </div>
                            <h2 class="entry-title">
                                <a href="#" rel="bookmark">The 2017 Wine Lover’s Guide to a Kitchen Remodel</a>
                            </h2>
                            <p>Lorem ipsum dolor sit amet scelerisque orci. Aenean et ex ut elit tincidunt rutrum vitae eleifend metusareil Nunc tincidunt venenatis tellus euismod fermentum. Maecenas sed dapibus eros. Phasellus eu mi metusdajn. Nunc mi nisl, viverra id sollicitudin et, auctor sit amet augue.</p>

                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="axtronic-post">
                        <div class="post-thumbnail">
                            <div class="posted-on-square">
                                <b>19</b>Feb
                            </div>
                            <a href="">
                                <img src="{{ theme_url('Axtronic/images/new3.png') }}" alt="Axtronic WooCommerce">
                            </a>
                        </div>
                        <div class="entry-content">
                            <div class="entry-meta">
                                <div class="post-author">
                                    <span class="label">By </span>
                                    <a href="#" rel="author">
                                        <span class="author_name">admin </span>
                                    </a>
                                </div>
                                <div class="meta-categories">
                                    <a href="#" rel="category tag">Tips &amp; Tricks</a>,
                                    <a href="" rel="category tag">Uncategorized</a>
                                </div>
                            </div>
                            <h2 class="entry-title">
                                <a href="#" rel="bookmark">The 2017 Wine Lover’s Guide to a Kitchen Remodel</a>
                            </h2>
                            <p>Lorem ipsum dolor sit amet scelerisque orci. Aenean et ex ut elit tincidunt rutrum vitae eleifend metusareil Nunc tincidunt venenatis tellus euismod fermentum. Maecenas sed dapibus eros. Phasellus eu mi metusdajn. Nunc mi nisl, viverra id sollicitudin et, auctor sit amet augue.</p>

                        </div>
                    </div>
                </div>
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>
