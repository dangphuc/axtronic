<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 3/25/2022
 * Time: 4:19 PM
 */
?>
<div class="axtronic-brands">
    <div class="container">
        <div class="swiper-slider-brands swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="item-brand">
                        <a href="#"><img src="{{ theme_url('Axtronic/images/brand-1.svg') }}" class="size-full" alt="Axtronic WooCommerce" ></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-brand">
                        <a href="#"><img src="{{ theme_url('Axtronic/images/brand-3.svg') }}" class="size-full" alt="Axtronic WooCommerce" ></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-brand">
                        <a href="#"><img src="{{ theme_url('Axtronic/images/brand-2.svg') }}" class="size-full" alt="Axtronic WooCommerce" ></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-brand">
                        <a href="#"><img src="{{ theme_url('Axtronic/images/brand-4.svg') }}" class="size-full" alt="Axtronic WooCommerce" ></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-brand">
                        <a href="#"><img src="{{ theme_url('Axtronic/images/brand-5.svg') }}" class="size-full" alt="Axtronic WooCommerce" ></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-brand">
                        <a href="#"><img src="{{ theme_url('Axtronic/images/brand-6.svg') }}" class="size-full" alt="Axtronic WooCommerce" ></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
