<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 3/21/2022
 * Time: 8:59 PM
 */
?>
<section id="comments" class="comments-area">
    <div class="clearfix">
        <div class="comment-list-wrap">
            <h3 class="title-comment">
                3 Responses
            </h3>
            <ol class="comment-list">
                <li class="comment">
                    <div class="comment-body">
                        <div class="comment-meta ">
                            <div class="comment-author vcard">
                                <img alt="" src="https://secure.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028?s=128&d=mm&r=g" class="avatar avatar-128 photo lazyloaded" height="128" width="128"/>
                                <cite class="fn">Elicia</cite>
                            </div>
                            <a href="#" class="comment-date">
                                <time datetime="2021-12-23T00:59:15+00:00">December 23, 2021</time>
                            </a>
                        </div>
                        <div id="div-comment-134" class="comment-content">
                            <div class="comment-text">
                                <p>This is exactly what i was looking for, thank you so much for these tutorials</p>
                            </div>
                            <div class="reply">
                                <a rel="nofollow" class="comment-reply-link" href="#" >Reply</a>
                            </div>
                        </div>
                    </div>
                    <ol class="children">
                        <li class="comment odd">
                            <div class="comment-body">
                                <div class="comment-meta">
                                    <div class="comment-author vcard">
                                        <img alt="" src="https://secure.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028?s=128&d=mm&r=g" class="avatar" height="128" width="128"/><cite class="fn">Elicia</cite> </div>
                                    <a href="#" class="comment-date">
                                        <time >December 23, 2021</time> </a>
                                </div>
                                <div id="div-comment-135" class="comment-content">
                                    <div class="comment-text">
                                        <p>It would be great to try this theme for my businesses</p>
                                    </div>
                                    <div class="reply">
                                        <a rel="nofollow" class="comment-reply-link" href="#" >Reply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ol>
                </li>
                <li class="comment ">
                    <div class="comment-body">
                        <div class="comment-meta">
                            <div class="comment-author">
                                <img alt="" src="https://secure.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028?s=128&d=mm&r=g" class="avatar avatar-128 photo lazyloaded" height="128" width="128"/><cite class="fn">Elicia</cite> </div>
                            <a href="#" class="comment-date">
                                <time datetime="2021-12-23T00:59:15+00:00">December 23, 2021</time> </a>
                        </div>
                        <div id="div-comment-136" class="comment-content">
                            <div class="comment-text">
                                <p>What a nice article. It keeps me reading more and more!</p>
                            </div>
                            <div class="reply">
                                <a rel="nofollow" class="comment-reply-link" href="#" >Reply</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ol>
        </div>
        <div id="respond" class="comment-respond">
            <h2 id="reply-title" class="comment-reply-title">
                Leave a Reply
            </h2>
            <p>Your email address will not be published. Required fields are marked *</p>
            <form action="" method="post" id="commentform" class="comment-form" novalidate="">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""> {{ __('Name') }} <span class="required">*</span></label>
                            <input type="text" value="" placeholder=" " name="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">{{ __('Email') }}  <span class="required">*</span></label>
                            <input type="text" value="" placeholder="" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">{{ __('Website') }}  <span class="required">*</span></label>
                            <input type="text" value="" placeholder="" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12 my-3">
                        <div class="form-group">
                            <label for="">{{ __('Comment ') }}</label>
                            <textarea name="message" cols="45" rows="8" class="textarea" placeholder=""></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 ">
                        <p class="comment-form-cookies-consent">
                            <input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes">
                            <label for="wp-comment-cookies-consent">Save my name, email, and website in this browser for the next time I comment.</label>
                        </p>
                    </div>
                    <div class="col-md-12 ">
                        <button class="submit btn " type="submit">
                            {{ __('Post Comment') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</section>
