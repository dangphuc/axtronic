<!-- header top -->
<div class="header_top bgc-thm2 dn-992">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-4">
                <div class="ht_contact_widget">
                    <ul class="m0">
                        <li class="list-inline-item"><a href="#"><span class="flaticon-phone-call mr5"></span> (+035) 527-1710-70</a></li>
                        <li class="list-inline-item"><a href="#"><span class="flaticon-email mr5"></span> order@freshen.com</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-xl-4">
                <div class="ht_contact_widget text-center">
                    <a class="text-white" href="#">Free shipping for orders over $59. $5.00 USPS Shipping on $25+ !</a>
                </div>
            </div>
            <div class="col-lg-4 col-xl-4">
                <div class="ht_language_widget text-end">
                    <ul class="m0">
                        <li class="list-inline-item">
                            <div class="htlw_form_select">
                                <span class="stts">Language</span>
                                <select class="custom_select_dd" id="selectbox_language">
                                    <option value="english">English</option>
                                    <option value="turkish">Turkish</option>
                                    <option value="turkish">للغة العربية </option>
                                    <option value="español">Español</option>
                                    <option value="italiano">Italiano</option>
                                </select>
                            </div>
                        </li>
                        <li class="list-inline-item">
                            <div class="htcw_form_select">
                                <span class="stts">Currency</span>
                                <select class="custom_select_dd" id="selectbox_currency">
                                    <option value="usd ($)">USD ($)</option>
                                    <option value="euro (€)">EURO (€)</option>
                                    <option value="pound (£)">Pound (£)</option>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
