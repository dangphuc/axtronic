<div class="bravo_callAaction" style="background: url({{ get_file_url($bg, 'full') }}) no-repeat">
    <div class="container">
        <h2 style="font-size: 48px;color: #ffffff;line-height: 1.1;text-align: center" class="vc_custom_heading semibold">
            {!! clean($title) !!}
        </h2>
        <div class="martfury-button text-center size-large color-dark">
            <a href="#register" data-toggle="modal" data-target="#register">{{__('Start Selling')}}</a>
        </div>
    </div>
</div>
