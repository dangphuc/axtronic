<?php
namespace Modules\Template\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Modules\Media\Helpers\FileHelper;

class BannerHome1 extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'banner_big_title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Banner big title')
                ],
                [
                    'id'        => 'banner_big_desc',
                    'type'      => 'input',
                    'inputType' => 'textArea',
                    'label'     => __('Banner big desc')
                ],


                [
                    'id'          => 'sliders',
                    'type'        => 'listItem',
                    'label'       => __('Slider Items'),
                    'title_field' => 'title',
                    'settings'    => [
                        [
                            'id'    => 'image',
                            'type'  => 'uploader',
                            'label' => __('Image Uploader')
                        ]
                    ]
                ],
                [
                    'id'          => 'saleOff',
                    'type'        => 'listItem',
                    'label'       => __('saleOff Items'),
                    'title_field' => 'title',
                    'settings'    => [
                        [
                            'id'        => 'title',
                            'type'      => 'input',
                            'inputType' => 'textArea',
                            'label'     => __('Title')
                        ],
                        [
                            'id'        => 'sub_title',
                            'type'      => 'input',
                            'inputType' => 'textArea',
                            'label'     => __('Sub Title')
                        ],
                        [
                            'id'        => 'number',
                            'type'      => 'input',
                            'inputType' => 'text',
                            'label'     => __('Sale off Number')
                        ],
                        [
                            'id'    => 'image',
                            'type'  => 'uploader',
                            'label' => __('Image Uploader')
                        ],
                        [
                            'id'        => 'link',
                            'type'      => 'input',
                            'inputType' => 'text',
                            'label'     => __('Link sale Off')
                        ],
                        [
                            'id'        => 'color',
                            'type'      => 'input',
                            'inputType' => 'text',
                            'label'     => __('Background sale Off')
                        ],
                    ]
                ]
            ]
        ]);
    }

    public function getName()
    {
        return __('Banner Home 1');
    }

    public function content($model = [])
    {
        return view('Template::frontend.blocks.BannerHome1.index', $model);
    }
}
