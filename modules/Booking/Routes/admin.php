<?php
use \Illuminate\Support\Facades\Route;

Route::get('orders','OrderController@index')->name('booking.admin.orders');
Route::post('orders/bulkEdit','OrderController@bulkEdit')->name('booking.admin.order.bulkEdit');
