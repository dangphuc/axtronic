<div class="header-style-default">
    <div class="bravo-container martfury-container">
        @include('Layout::headers.parts.main-header')
    </div>
    <div class="bravo-main-menu-wrap">
        <div class="bravo-container martfury-container">
            @include('Layout::headers.parts.main-menu')
        </div>
    </div>
</div>
