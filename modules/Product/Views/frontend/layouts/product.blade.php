<?php
$reviewData = $row->getScoreReview();
$score_total = $reviewData['score_total'];
?>

<li class="product {{ $score_total > 0 ? 'is_review' : '' }}">
    <div class="product-inner">
        <div class="mf-product-thumbnail">
            <a href="{{$row->getDetailUrl()}}">
                {!! get_image_tag($row->image_id,'thumb',['lazy'=>true,'alt'=>$row->title]) !!}
            </a>
            <div class="footer-button">
                @php $in_stock = $row->stock_status == 'in' @endphp
                @if($row->product_type == 'simple')
                    <a href="{{ $in_stock ? '#' : $row->getDetailUrl() }}" class="add_to_cart {{ $in_stock ? 'bravo_add_to_cart' : '' }}" data-product='{"id":{{$row->id}},"type":"simple"}'>
                        <i class="p-icon icon-bag2" data-toggle="tooltip" title="{{ $in_stock ? __("Add to cart") : __("Read more") }}"></i>
                    </a>
                @else
                    <a href="{{ $row->getDetailUrl() }}" class="add_to_cart">
                        <i class="p-icon icon-bag2" data-toggle="tooltip" title="{{ __('Select options') }}"></i>
                    </a>
                @endif

                <a href="#" class="mf-product-quick-view" data-toggle="tooltip" title="{{__('Quick View')}}" data-product={"id":{{$row->id}},"type":"{{$row->type}}"}>
                    <i class="p-icon icon-eye"></i>
                </a>
                @php $hasWishList = $row->hasWishList; @endphp
                <div class="yith-wcwl-add-to-wishlist service-wishlist {{ $hasWishList ? 'active' : '' }}" data-id="{{ $row->id }}" data-type="{{ $row->type }}" data-toggle="tooltip" title="{{ $hasWishList ? __('Browse to Wishlist') : __('Add to Wishlist')}}">
                    <div class="yith-wcwl-add-button">
                        <a href="{{route('user.wishList.index')}}" class="wishlist_link">
                            <i class="p-icon icon-heart"></i>
                        </a>
                    </div>
                </div>
                <div class="compare-button mf-compare-button {{ in_array($row->id, list_compare_id()) ? 'browse' : '' }}" data-toggle="tooltip" title="{{ in_array($row->id, list_compare_id()) ? __('Browse Compare') : __('Compare') }}" data-id="{{$row->id}}">
                    <a href="#" class="compare">
                        <i class="p-icon icon-chart-bars"></i>
                    </a>
                </div>
            </div>

            <span class="ribbons">
            @if($row->stock_status == "in")
                @if(!empty($row->discount_percent))
                    <span class="onsale ribbon">
                        <span class="sep">-</span>{{$row->discount_percent}}
                    </span>
                @endif
            @else
                <span class="out-of-stock ribbon">{{__('Out Of Stock')}}</span>
            @endif
            </span>

        </div>

        <div class="mf-vendor-name">
            <div class="sold-by-meta">
                @php $link_search_brand = Modules\Product\Models\Product::getLinkForPageSearch(false , [ 'brand[]' => $row->brand_id] ); @endphp
                <a href="{{$link_search_brand}}">{{$row->brand['name']}}</a>
            </div>
        </div>
        <div class="product-price">
            @include('Product::frontend.details.price')
        </div>

        <h2>
            <a href="{{$row->getDetailUrl()}}">{{$row->title ?? ''}}</a>
        </h2>
        @if($brand = $row->brand->name ?? "")
            <div class="sold-by-meta">
                <span class="sold-by-label">{{__('Brand: ')}}</span>
                <span>{{$brand}}</span>
            </div>
        @endif

        @if($score_total)
            <div class="service-review tour-review-{{$score_total}}">
                <div class="list-star">
                    <ul class="booking-item-rating-stars">
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                    </ul>
                    <div class="booking-item-rating-stars-active"
                         style="width: {{  $score_total * 2 * 10 ?? 0  }}%">
                        <ul class="booking-item-rating-stars">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                    </div>
                </div>
                <span class="review">
                    @php $total_review = ($reviewData['total_review'] < 10) ? '0'.$reviewData['total_review'] : $reviewData['total_review'] @endphp
                    <span class="review_number">{{ $total_review }}</span>
                </span>
            </div>
        @endif
    </div>
</li>
