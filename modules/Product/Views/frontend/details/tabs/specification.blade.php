<div class="tabs-panel">
    <table class="shop_attributes">
        <tbody>
            <tr class="product-attributes-item product-attributes-item--attribute_pa_color">
                <th class="product-attributes-item__label">Color</th>
                <td class="product-attributes-item__value"><p>Black, Gray</p>
                </td>
            </tr>
            <tr class="product-attributes-item product-attributes-item--attribute_pa_sound-stype">
                <th class="product-attributes-item__label">Stype</th>
                <td class="product-attributes-item__value"><p>Ear Hook</p>
                </td>
            </tr>
            <tr class="product-attributes-item product-attributes-item--attribute_pa_wireless">
                <th class="product-attributes-item__label">Wireless</th>
                <td class="product-attributes-item__value"><p>Yes</p>
                </td>
            </tr>
            <tr class="product-attributes-item product-attributes-item--attribute_dimensions">
                <th class="product-attributes-item__label">Dimensions</th>
                <td class="product-attributes-item__value"><p>5.5 x 5.5 x 9.5 inches</p>
                </td>
            </tr>
            <tr class="product-attributes-item product-attributes-item--attribute_weight">
                <th class="product-attributes-item__label">Weight</th>
                <td class="product-attributes-item__value"><p>6.61 pounds</p>
                </td>
            </tr>
            <tr class="product-attributes-item product-attributes-item--attribute_battery-life">
                <th class="product-attributes-item__label">Battery Life</th>
                <td class="product-attributes-item__value"><p>20 hours</p>
                </td>
            </tr>
            <tr class="product-attributes-item product-attributes-item--attribute_bluetooth">
                <th class="product-attributes-item__label">Bluetooth</th>
                <td class="product-attributes-item__value"><p>Yes</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
